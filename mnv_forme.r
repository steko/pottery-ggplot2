## mnv_forme.r
## Author: Stefano Costa <steko@unisi.it>
##
## This script plots bars for the Minimum Number of Vessels of each type
## found in the data file.
##
## There are a number of assumptions about the fields in the CSV file,
## but they should be fairly simple to achieve.

library(ggplot2)
tsa <- read.csv('tabella_quantitativa_Andujar.csv',
                header=T,
                as.is=c(T,F,F,F,F,T))

ggplot(tsa,
       aes(x = reorder(Tipo,Ordine),
           y = ceil(mean(numero.min, numero.max))#,
           #fill=Classe)
       ) +
  geom_bar() +
  geom_text(aes(y = numero.min + max(numero.min)*0.1, label = numero.min, size=2)) +
  coord_flip() +
  theme_bw() +
  scale_fill_grey() +
  xlab("Tipi") +
  ylab("Numero minimo di vasi") +
  opts(title = "Numero minimo di vasi per tipo")

ggsave('mnv_forme.pdf')
