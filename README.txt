These scripts use the R ggplot2 library for plotting meaningful
quantitative data about archaeological pottery.

The ``weight-density`` directory contains functions that work with weight
data and related measurements.
