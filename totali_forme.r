## totali_forme.r
## Author: Stefano Costa <steko@unisi.it>
##
## This script plots bars with total number of sherds per type.

library(ggplot2)
tsa <- read.csv('tabella_quantitativa_Andujar.csv',
                header=T,
                as.is=c(T,F,F,F,F,T))

ggplot(tsa,
       aes(x = reorder(Tipo,Ordine), y = totale)) +
  geom_bar() +
  geom_text(aes(y = totale + 5, label = totale, size=2)) +
  coord_flip() +
  theme_bw() +
  scale_fill_grey() +
  xlab("Tipi") +
  ylab("Frammenti totali") +
  opts(title = "Numero totale di frammenti per tipo")

ggsave('totali_forme.pdf')
