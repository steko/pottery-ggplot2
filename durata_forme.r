library(ggplot2)
tsa <- read.csv('durata_forme.csv', header=T)

tsa <- na.omit(tsa)

ggplot(tsa) +
  geom_segment(aes(x=cron_inizio, xend=cron_fine,
                   y=reorder(Tipo,cron_inizio),
                   yend=reorder(Tipo,cron_inizio)),
               alpha = 1, size=2, fill="#ff0000") +
  opts(title="Arco cronologico dei tipi") +
  xlab("Cronologia") +
  ylab("Tipi") +
  theme_bw()

ggsave('durata_forme.pdf')
