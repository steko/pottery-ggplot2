## This is a literate script, a sort of step by step guide.
##
## You start from CSV files (because they are easy to create from a spreadsheet).

us1032 <- read.csv("us1032.csv", header=TRUE)
us907 <- read.csv("us907.csv", header=TRUE)

## The two files were created in different moments, so they only share the "Weight"
## variable. The optimal situation is when all your data were collected with the same
## variables.

us1032$US <- rep("1032", length(us1032$Peso)) # both data frames are missing this
us907$US <- rep("907", length(us907$Peso))    # and it's needed before binding

us1032$Spessore <- rep(NA, length(us1032$Peso)) # context 1032 is missing sherd thickness

us907$Classe <- rep(NA, length(us907$Peso)) # context 907 is missing classification data

## Now we are ready to bind the two data frames together

ambiente27 <- rbind(us1032,us907)

## A good visualisation is as follows

library(ggplot2)                        # required

ggplot(
       ambiente27,
       aes(log(Peso))                   # weight distribution is log-normal or very close to it
       ) +
  geom_histogram(binwidth=0.5) +
  facet_wrap( ~ US, ncol=1)
